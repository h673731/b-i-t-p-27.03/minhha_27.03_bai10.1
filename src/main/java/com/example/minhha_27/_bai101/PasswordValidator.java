package com.example.minhha_27._bai101;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<ValidPassword, String> {
    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        if (password == null) {
            return false;
        }

        if (password.length() < 8) {
            return false;
        }

        if (Character.isDigit(password.charAt(0))) {
            return false;
        }

        if (!password.matches("[a-zA-Z0-9]+")) {
            return false;
        }

        return true;
    }
}
