package com.example.minhha_27._bai101;

import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class controller {
    @PostMapping("/validatePassword")
    public ResponseEntity<String> validatePassword(@Valid @RequestBody PasswordDto passwordDto) {
        return ResponseEntity.ok("Mật khẩu chính xác");
    }
}
