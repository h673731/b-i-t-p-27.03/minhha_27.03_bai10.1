package com.example.minhha_27._bai101;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class PasswordDto {
    @ValidPassword
    private String password;
}
